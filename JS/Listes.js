
var comboAuteur = document.FormListes.ListeAuteur;
var comboSerie = document.FormListes.ListeSerie;
var idAuteurSelectionne = 0;
var idSerieSelectionne = 0;

function remplissageComboAuteur(value) {
    comboAuteur.length++;
    comboAuteur.options[comboAuteur.length - 1].text = value.nom;
}
auteurs.forEach(remplissageComboAuteur);

function remplissagecomboSerie(value) {
    comboSerie.length++;
    comboSerie.options[comboSerie.length - 1].text = value.nom;
}
series.forEach(remplissagecomboSerie);

function RechercheIdAuteursSelectionne(value, key) {  
    if (value.idAuteur == idAuteurSelectionne) { 
        idAuteurSelectionne = key;
    }
}

function RechercheIdSerieSelectionne(value, key) {  
    if (value.idSerie == idSerieSelectionne) { 
        idSerieSelectionne = key;
    }
}

var maListeDesSeries = document.getElementById("idSerie");
maListeDesSeries.addEventListener("change", function () {
		getListeSerie(this)
    });
    
    function getListeSerie(idSerie) {

		var album = albums.get(num.value);

		if (album === undefined) {
			txtSerie.value = "";
			txtNumero.value = "";
			txtTitre.value = "";
			txtAuteur.value = "";
			txtPrix.value = 0;

			afficheAlbums($("#albumMini"), $("#album"), albumDefaultMini, albumDefault);

		} else {

			var serie = series.get(album.idSerie);
			var auteur = auteurs.get(album.idAuteur);

			document.getElementById("nomSerie").innerHTML +=serie.nom;
				 document.getElementById("numAlbum").innerHTML +=album.numero;
				document.getElementById("nomAlbum").innerHTML +=album.titre;
			 document.getElementById("nomAuteur").innerHTML +=auteurs.get(album.idAuteur).nom;

			var nomFic = serie.nom + "-" + album.numero + "-" + album.titre;

			// Utilisation d'une expression régulière pour supprimer 
			// les caractères non autorisés dans les noms de fichiers : '!?.":$
			nomFic = nomFic.replace(/'|!|\?|\.|"|:|\$/g, "");

			afficheAlbums(
				$("#albumMini"),
				$("#album"),
				srcAlbumMini + nomFic + ".jpg",
				srcAlbum + nomFic + ".jpg"
			);

		}
	}


