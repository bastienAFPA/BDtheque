jQuery(document).ready(function ($) {
  const srcImg = "images/"; // emplacement des images de l'appli
  const albumDefaultMini = srcImg + "noComicsMini.jpeg";
  const albumDefault = srcImg + "noComics.jpeg";
  const srcAlbumMini = "albumsMini/"; // emplacement des images des albums en petit
  const srcAlbum = "albums/"; // emplacement des images des albums en grand

  var comboAuteur = document.FormListes.ListeAuteur;
  var comboSerie = document.FormListes.ListeSerie;
  var comboTitre = document.FormListes.ListeTitre;
  var comboAuteurSelect = document.getElementById("maListeAuteur");
  var comboSerieSelect = document.getElementById("maListeDesSeries");
  var comboTitreSelect = document.getElementById("maListeDesTitres");
  var imgAlbum = document.getElementById("album");
  var imgAlbumMini = document.getElementById("albumMini");
  var listegetAuteur = "";
  var listegetAlbums = 0;
  var listegetSerie = "";
  var monAffichage = document.getElementById("pouraffichage");
  var titreAlbumRechercher = "";
  var numeroSerieaRechercher = 0;
  var nomSerie = "";

  function remplissageComboAuteur(value) {
    comboAuteur.length++;
    comboAuteur.options[comboAuteur.length - 1].text = value.nom;
  }

  function remplissageComboSerie(value) {
    comboSerie.length++;
    comboSerie.options[comboSerie.length - 1].text = value.nom;
  }

  function remplissageComboTitre(value) {
    var validSerie = value.idAuteur;
    if (validSerie == listegetAuteur) {
      comboTitre.length++;
      comboTitre.options[comboTitre.length - 1].text = value.titre;
    }
  }

  function RecherchelistegetAuteur(value, key) {
    if (value.nom == listegetAlbums) {
      listegetAuteur = key;
    }
  }

  function RecherchelistegetSerie(value, key) {
    if (value.idSerie == listegetSerie) {
      listegetSerie = key;
    }
  }

  function getAuteur(element) {
    monAffichage.innerHTML += "auteur : " + element.value;
    listegetAlbums = element.value;
    auteurs.forEach(RecherchelistegetAuteur);
    comboTitre.length = 1;
    albums.forEach(remplissageComboTitre);
  }

  function getSerie(element) {
    monAffichage.innerHTML += "Serie : " + element.value;
    listegetSerie = element.value;
    auteurs.forEach(RecherchelistegetSerie);
    comboTitre.length = 1;
    series.forEach(remplissageComboTitre);
  }

  function getTitre(element) {
    monAffichage.innerHTML += " et le titre de la BD se nomme : " + element.value;
  }

  auteurs.forEach(remplissageComboAuteur);

  comboAuteurSelect.addEventListener("change", function () {
    monAffichage.innerHTML = "Vous avez choisi comme ";
    getAuteur(this);
  });

  series.forEach(remplissageComboSerie);

  comboSerieSelect.addEventListener("change", function () {
    monAffichage.innerHTML = "votre choix : ";
    getSerie(this);
  });

  comboTitreSelect.addEventListener("change", function () {
    getTitre(this);
  });

  // affichage image BD

  for (var [idAuteur, auteur] of auteurs.entries()) {
    // Recherche des albums de l'auteur
    for (var [idAlbum, album] of albums.entries()) {
      if (album.idAuteur == idAuteur) {
        // console.log(auteur.nom+", Album N°"+album.numero+" "+album.titre+", Série:"+series.get(album.idSerie).nom);
        // document.getElementById("recupListe").innerHTML += auteur.nom+", Album N°"+album.numero+" "+album.titre+", Série:"+series.get(album.idSerie).nom;
      }
    }
  }

  imgAlbum.addEventListener("error", function () {
    prbImg(this);
  });

  imgAlbumMini.addEventListener("error", function () {
    prbImg(this);
  });

  var id = document.getElementById("maListeDesTitres");
  id.addEventListener("change", function () {
    getAlbum(this);
  });

  function rechercheNomSerie(value, key) {
    if (numeroSerieaRechercher == key) {
      nomSerie = value.nom;
    }
  }

  function rechercheImageAffiche(value, key) {
    // alert(titreAlbumRechercher)
    if (value.titre === titreAlbumRechercher) {
      var monNumero = value.numero;
      // alert(monNumero);
      numeroSerieaRechercher = value.idSerie;
      series.forEach(rechercheNomSerie);
      // var nomSerie = "Marsupilami";

      // txtSerie.value = serie.nom;
      // txtNumero.value = album.numero;
      // txtTitre.value = album.titre;
      // txtAuteur.value = auteur.nom;
      // txtPrix.value = album.prix;

      var nomFic = nomSerie + "-" + monNumero + "-" + titreAlbumRechercher;

      // Utilisation d'une expression régulière pour supprimer
      // les caractères non autorisés dans les noms de fichiers : '!?.":$
      nomFic = nomFic.replace(/'|!|\?|\.|"|:|\$/g, "");

      afficheAlbums(
        $("#albumMini"),
        $("#album"),
        srcAlbumMini + nomFic + ".jpg",
        srcAlbum + nomFic + ".jpg"
      );
    }
  }

  /**
   * Récupération de l'album par son id et appel de
   * la fonction d'affichage
   *
   * @param {number} num
   */

  function getAlbum(texttitre) {
    titreAlbumRechercher = texttitre.value;
    albums.forEach(rechercheImageAffiche);
  }

  /**
   * Affichage des images, les effets sont chainés et traités
   * en file d'attente par jQuery d'où les "stop()) et "clearQueue()"
   * pour éviter l'accumulation d'effets si défilement rapide des albums.
   *
   * @param {object jQuery} $albumMini
   * @param {object jQuery} $album
   * @param {string} nomFic
   * @param {string} nomFicBig
   */
  function afficheAlbums($albumMini, $album, nomFicMini, nomFic) {
    $album
      .stop(true, true)
      .clearQueue()
      .fadeOut(100, function () {
        $album.attr("src", nomFic);
        $albumMini
          .stop(true, true)
          .clearQueue()
          .fadeOut(150, function () {
            $albumMini.attr("src", nomFicMini);
            $albumMini.slideDown(200, function () {
              $album.slideDown(200);
            });
          });
      });
  }

  /**
   * Affichage de l'image par défaut si le chargement de l'image de l'album
   * ne s'est pas bien passé
   *
   * @param {object HTML} element
   */
  function prbImg(element) {
    // console.log(element);
    if (element.id === "albumMini") element.src = albumDefaultMini;
    else element.src = albumDefault;
  }
});
